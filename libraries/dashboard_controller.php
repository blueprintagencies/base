<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * This controller provides some improved convenience methods for dashboard controllers
 *
 * @package base
 */

class DashboardController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->initCSRFToken();
        $this->initFlash();
    }

    private function initFlash()
    {
        $types = array(
            'message',
            'success',
            'error'
        );
        foreach ($types as $type) {
            $key = "flash_{$type}";
            if (!empty($_SESSION[$key])) {
                $this->set($type, $_SESSION[$key]);
                //C5 automagically displays 'message', 'success', and 'error' for us in dashboard views
                unset($_SESSION[$key]);
            }
        }
    }

    //Uses session to set 'message', 'success', or 'error' variable next time the page is loaded
    public function flash($text, $type = 'message')
    {
        $key = "flash_{$type}";
        $_SESSION[$key] = $text;
    }

    /**
     * Tokens must be provided when making post requests on our dashboard
     */
    private function initCSRFToken()
    {
        $token = Loader::helper('validation/token');
        if (!empty($_POST) && !$token->validate()) {
            die($token->getErrorMessage());
        }
        $this->set('token', $token->output('', true));
    }

    //Render a view file with the given name that exists in the single_pages
    // directory that corresponds with this controller's location / class name.
    public function render($view)
    {
        $path = $this->path($view);
        parent::render($path);
    }

    //Wrapper around View::url that always passes the controller's path as the url path
    // so you can call url('task', etc.) instead of url('path/to/controller', 'tasks', etc.).
    public function url($task = null)
    {
        //Do some fancy php stuff so we can accept and pass along
        // a variable number of args (anything after the $task arg).
        $args = func_get_args();
        array_unshift($args, $this->path());
        return call_user_func_array(array(
            'View',
            'url'
        ), $args);
    }

    //Redirect to an action in this controller
    public function redirect($action, $fullPathProvided = FALSE)
    {
        if ($fullPathProvided) {
            parent::redirect($action);
        }
        //Do some fancy php stuff so we can accept and pass along
        // a variable number of args (anything after the $action arg).
        $args = func_get_args();
        array_unshift($args, $this->path());
        call_user_func_array(array(
            'parent',
            'redirect'
        ), $args);
    }

    //Return this controller's page path, with optional other things appended to it.
    //Note that a controller's path is wherever the single_page lives
    // in the sitemap -- not a specific action or view that is being displayed.
    public function path($append = '')
    {
        $path = $this->getCollectionObject()->getCollectionPath();
        if (!empty($append)) {
            $path .= '/'.$append;
        }
        return $path;
    }

}
