<?php defined('C5_EXECUTE') or die('Access Denied');

/**
 * This package was designed to be installed with a fresh copy of concrete5.
 */

class BasePackage extends Package
{

    protected $pkgHandle = 'base';
    protected $appVersionRequired = '5.6';
    protected $pkgVersion = '1.1';

    /**
     * Format:
     *      array(
     *          'collection_handle' => "Collection Name"
     *      );
     */
    private $collectionTypes = array(
        'home' => 'Home',
        'slide' => 'Slide',
        'subpage' => 'Subpage',
    );
	
	private $autoLoadClasses = array(
        'SinglePageController' => array(
            'library',
            'single_page_controller',
            'base',
        ),
        'DashboardController' => array(
            'library',
            'dashboard_controller',
            'base',
        ),
        'DatabaseObject' => array(
            'model',
            'database_object',
            'base',
        ),
        'DatabaseList' => array(
            'model',
            'database_list',
            'base',
        ),
        'PackageHelper' => array(
            'helper',
            'package',
            'base',
        ),
    );
    
    /**
     * Format:
     *      array(
     *          '/unique/url' => array('attrib_handle' => [attribute value], ...)
     *      );
     * 
     * NOTE: with the exception of the attribute handle: cName. This handle will set the
     *      single page name. (just technically not an attribute)
     */
    private $singlePages = array(
		/* example dashboard page
		'/dashboard/admin/your_page' => array(
            'icon_dashboard' => 'icon-briefcase',
            'cName' => 'Event Administration'
        ),*/
		
		/* example public page 
		'/your_page' => array('exclude_nav' => false),
		*/
	);

    /**
     * Format: array('theme_handle_1', 'theme_handle_2');
     */
    private $themes = array('base');

    /**
     * Format:
     *      array(
     *          "Group Name" => "Group Description",
     *      );
     */
    private $userGroups = array();

    /**
     * Format:
     *      array(
     *          'attrib_set_handle' => "Attribute Set Name"
     *      );
     */
    private $attributeSets = array('base_attributes' => "Base Attributes");

    /**
     * Format:
     *      array(
     *          'collection_handle' => "Collection Name"
     *      );
     */
    private $cAttributes = array(
        'slide' => array(
            array(
                'attribute_set' => 'base_attributes',
                'type' => 'text',
                'akHandle' => 'slide_url',
                'akName' => 'Slide URL',
            ),
            array(
                'attribute_set' => 'base_attributes',
                'type' => 'image_file',
                'akHandle' => 'slide_image',
                'akName' => 'Slide Image',
            ),
        ), );

    public function getPackageDescription()
    {
        return t("Base developer concrete5 package");
    }

    public function getPackageName()
    {
        return t("Base Package");
    }

    public function install()
    {
        $pkg = parent::install();
        $this->configure();
    }

    public function upgrade()
    {
        $result = parent::upgrade();
        $this->configure();
    }

    public function configure()
    {
        $pkg = Package::getByHandle($this->pkgHandle);
        $this->addCollectionTypes($pkg);
        $this->addAttributeSets($pkg);
        $this->addCollectionAttributes($pkg);
        $this->addSinglePages($pkg);
        $this->addThemes($pkg);
        $this->addUserGroups($pkg);
    }

    private function addCollectionTypes($pkg)
    {
        Loader::model('collection_types');
        if (is_array($this->collectionTypes) && !empty($this->collectionTypes)) {
            foreach ($this->collectionTypes as $handle => $name) {
                $ct = CollectionType::getByHandle($handle);
                if (!is_object($ct)) {
                    CollectionType::add(array(
                        'ctName' => $name,
                        'ctHandle' => $handle
                    ), $pkg);
                }
            }
        }
    }

    private function addAttributeSets($pkg)
    {
        if (is_array($this->attributeSets) && !empty($this->attributeSets)) {
            foreach ($this->attributeSets as $asHandle => $asName) {
                $asObj = AttributeSet::getByHandle($asHandle);
                if (!is_object($asObj)) {
                    // AttributeSet does not exist
                    $eaku = AttributeKeyCategory::getByHandle('collection');
                    $eaku->setAllowAttributeSets(AttributeKeyCategory::ASET_ALLOW_SINGLE);
                    $eaku->addSet($asHandle, t($asName), $pkg);
                }
            }
        }
    }

    private function addCollectionAttributes($pkg)
    {
        if (is_array($this->cAttributes) && !empty($this->cAttributes)) {
            foreach ($this->cAttributes as $cHandle => $attribs) {
                $collection = CollectionType::getByHandle($cHandle);
                // We can only proceed if the collection exists. If it doesn't it was not supplied
                // in the packages $collectionTypes array
                if (is_object($collection)) {
                    // Now that collection exists, add attributes to it
                    if (is_array($attribs) && !empty($attribs)) {
                        foreach ($attribs as $attribData) {
                            $as = AttributeSet::getByHandle($attribData['attribute_set']);
                            $cak = CollectionAttributeKey::getByHandle($attribData['akHandle']);
                            // The attribute we are adding cannot exist already, and the attribute set
                            // we are adding the attribute to, MUST exist
                            if (!is_object($cak) && is_object($as)) {
                                CollectionAttributeKey::add($attribData['type'], array(
                                    'akHandle' => $attribData['akHandle'],
                                    'akName' => t($attribData['akName']),
                                ), $pkg)->setAttributeSet($as);

                                $new_cak = CollectionAttributeKey::getByHandle($attribData['akHandle']);

                                // Now that we have added the new collection attribute, we must assign to
                                // the collection object
                                $collection->assignCollectionAttribute($new_cak);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Add your new single pages to the $singlePages property of this class.
     * @param (Package Object) $pkg - Package to install single pages too
     * @return void
     */
    private function addSinglePages($pkg)
    {
        Loader::model('single_page');
        $cak = CollectionAttributeKey::getByHandle('icon_dashboard');
        foreach ($this->singlePages as $path => $attribs) {
            $sp = SinglePage::getByPath($path);
            if (!intval($sp->cID)) {
                $sp = SinglePage::add($path, $pkg);
            }

            // If we are adding a new single page, we won't have errors
            if (is_object($sp) && (!$sp->isError() !== FALSE)) {
                if (is_array($attribs) && !empty($attribs)) {
                    foreach ($attribs as $ak => $val) {
                        // cName is handled differently as it is not an attribute
                        if ($ak == 'cName') {
                            $sp->update(array('cName' => t($val)));
                            continue;
                        }
                        $sp->setAttribute($ak, $val);
                    }
                }
            }
        }
    }

    private function addThemes($pkg)
    {
        // Installed theme from ./theme folder
        if (is_array($this->themes) && !empty($this->themes)) {
            foreach ($this->themes as $themeHandle) {
                $pt = PageTheme::getByHandle($themeHandle);
                if (!is_object($pt)) {
                    PageTheme::add($themeHandle, $pkg);
                }
            }
        }
    }

    private function addUserGroups($pkg)
    {
        if (is_array($this->userGroups) && !empty($this->userGroups)) {
            foreach ($this->userGroups as $gName => $gDescription) {
                if (!is_object(Group::getByName($gName))) {
                    Group::add($gName, $gDescription);
                }
            }
        }
    }

    public function on_before_render()
    {
        $page = Page::getCurrentPage();

        if (!is_object($page)) {
            return;
        }

        $html = Loader::helper('html');
        $view = View::getInstance();
        /*$view->addHeaderItem($html->javascript(DIR_REL.'/packages/base/js/calls.js'));
		$view->addHeaderItem($html->css(DIR_REL.'/packages/base/themes/base/css/dashboard.css'));*/
    }

    /**
     * Uncomment below to load special classes.
     *
     * @todo
     */
    public function on_start()
    {
        Loader::registerAutoload($this->autoLoadClasses);
		
		// Required to give our package controller the on_before_render method
        $packageDirectory = DIR_PACKAGES.'/'.$this->pkgHandle.'/controller.php';
        Events::extend('on_before_render', 'BasePackage', 'on_before_render', $packageDirectory);
    }
}
