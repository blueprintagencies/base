<?php defined('C5_EXECUTE') or die('Access Denied');

class ThemeHelper
{

	/**
	 * Creates a bunch of useful class names for use within CSS/JS
	 *
	 * @author Nathan Krueger
	 * @return (string) $classes - Formatted properly to fit within an HTML attribute value
	 */
	public static function getBodyClasses()
	{
		$classes = array();

		// Indicates whether the user is logged in or not
		$u = new User();
		if ($u->isLoggedIn()) {
			$classes[] = 'loggedIn';
			// Are they a super user?
			if ($u->superUser) {
				$classes[] = 'superUser';
			}
		} else {
			$classes[] = 'notLoggedIn';
		}
		// Instanciate concrete5's global object
		global $c;
		// In concrete5's editor or not
		if ($c->isEditMode()) {
			$classes[] = 'editMode';
		} else {
			$classes[] = 'notEditMode';
		}

		// On front page or not
		if ($c->getCollectionID() == HOME_CID) {
			$classes[] = 'front';
		} else {
			$classes[] = 'notFront';
		}

		// Shows the pages type
		$classes[] = 'pageType_' . $c->getCollectionTypeHandle();

		//Shows the pages' ID
		$classes[] = 'pageCID_' . $c->getCollectionID();

		// Separate strings with spaces
		return implode(' ', $classes);
	}

}
