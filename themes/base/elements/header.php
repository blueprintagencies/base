<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="<?php echo LANGUAGE?>" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php Loader::element('header_required'); ?>

<?php
/**
 * NOTE! If you want the style sheets to be passed through concrete5's CSS processor
 *      in order to allow theme color manipulation from the dashboard, you must use:
 * 
 *              href="<?= $this->getStyleSheet('css/MY_STYLE_SHEET.css')?>"
 */
?>
<!-- Site Styles //-->
<!-- Font Awesome CSS
<link rel="stylesheet" media="screen" type="text/css" href="<?= $this->getThemePath();?>/css/font-awesome.css" />
-->
<!-- Bootstrap CSS -->
<link rel="stylesheet" media="screen" type="text/css" href="<?= $this->getThemePath();?>/css/bootstrap.css" />

<!-- Flex Slider CSS -->
<link rel="stylesheet" media="screen" type="text/css" href="<?= $this->getThemePath();?>/css/flexslider.css" />


<!-- Meanmenu CSS -->
<link rel="stylesheet" media="screen" type="text/css" href="<?= $this->getThemePath();?>/css/meanmenu.min.css" />


<link rel="stylesheet" media="screen" type="text/css" href="<?= $this->getThemePath();?>/css/main.css" />
<link rel="stylesheet" media="screen" type="text/css" href="<?= $this->getThemePath();?>/css/phone.css" />
<link rel="stylesheet" media="screen" type="text/css" href="<?= $this->getThemePath();?>/css/typography.css" />
<!-- responsive plugin for ie8 and earlier -->
<script type="text/javascript" src="<?= $this->getThemePath();?>/scripts/respond.min.js"></script>

<!-- Site Scripts //-->
<!-- Bootstrap JS -->
<script type="text/javascript" src="<?= $this->getThemePath();?>/scripts/bootstrap.min.js"></script>

<!-- Flex Slider JS -->
<script type="text/javascript" src="<?= $this->getThemePath();?>/scripts/jquery.flexslider-min.js"></script>

<!-- Responsive Meanmenu JS-->
<script type="text/javascript" src="<?= $this->getThemePath();?>/scripts/jquery.meanmenu.min.js"></script>

<script type="text/javascript" src="<?= $this->getThemePath();?>/scripts/calls.js"></script>

<!--[if IE]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>
<?php
// Create a string of useful classes for the body tag
Loader::helper('theme', 'base');

?>
<body class="<?= ThemeHelper::getBodyClasses(); ?>">
	<header>
    	<div class="container">
            <div class="row">
                <div class="col-md-3 logo-wrapper">
                    <a id="logo" href="<?= DIR_REL;?>/">
                        <img src="<?= $this->getThemePath();?>/images/logo.png" />
                    </a>
                </div>
                <div class="col-md-9 nav-wrapper">
                    <nav id="mainNav">
                    <?php
                    $nav = BlockType::getByHandle('autonav');
                    $nav->controller->orderBy = 'display_asc';
                    $nav->controller->displayPages = 'top';
                    $nav->controller->displaySubPages = 'all';
                    $nav->controller->displaySubPageLevelsNum = 2;
                    $nav->controller->displaySubPageLevels = 'custom';
                    $nav->render('templates/main_nav');
                    ?>
                    </nav>
                </div>
                <div id="mobileNav">
                </div>    
            </div>
        </div>
	</header>
		