<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>

	<div class="container"
    	<div class="row">
            <div id="sidebar" class="col-md-4">
                <?php 
                $as = new Area('Sidebar');
                $as->display($c);
                ?>		
            </div>  
            <div id="body" class="col-md-8">	
                <?php 
                $a = new Area('Main');
                $a->display($c);
                ?>
            </div>
		
			<div class="spacer">&nbsp;</div>	
         </div>	
	</div>

<?php  $this->inc('elements/footer.php'); ?>