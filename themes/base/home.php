<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>

<div id="home" class="container">
	<div id="main" class="row">
        <div class="col-md-12">
            <?php
            $a = new Area('Main');
            $a -> display($c);
            ?>		
        </div>
    </div>
</div>

<?php  $this->inc('elements/footer.php'); ?>