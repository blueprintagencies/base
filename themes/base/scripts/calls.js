$(document).ready(function(){
	// Base Slider
	if( $('.base-slider').length )
	{
		 // it exists
		$('.base-slider').flexslider({
			prevText: "",
			nextText: "",
		});
	}
	
	$('#mainNav').meanmenu({meanMenuContainer: '#mobileNav',
						    meanScreenWidth: "768",
						    meanMenuClose: "<span></span><span></span><span></span>"});
});
