<?php 
defined('C5_EXECUTE') or die("Access Denied.");

$th = Loader::helper('text');
$ih = Loader::helper('image'); //<--uncomment this line if displaying image attributes (see below)
//$dh = Loader::helper('date'); //<--uncomment this line if displaying dates (see below)
//Note that $nh (navigation helper) is already loaded for us by the controller (for legacy reasons)
?>

<div class="base-slider flexslider">
	<ul class="slides">
	<?php  foreach ($pages as $page):
		// Prepare data for each page being listed...
		$title = $th->entities($page->getCollectionName());
		$url = $nh->getLinkToCollection($page);
		$target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
		$target = empty($target) ? '_self' : $target;
		$description = $page->getCollectionDescription();
		$description = $controller->truncateSummaries ? $th->shorten($description, $controller->truncateChars) : $description;
		$description = $th->entities($description);	
		$img = $page->getAttribute('slide_image');
		if(!is_object($img)){
			// Image hasn't been set, so skip this page
			continue;
		}
		$img_src = $img->getRelativePath();
		?>
		<li class="slide">
			<img src="<?php echo $img_src ?>"  alt="Slide Image" />
		</li>
		
	<?php  endforeach; ?>
 	</ul>
</div>

