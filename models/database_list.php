<?php defined('C5_EXECUTE') or die('Access Denied.');

/**
 * The base DatabaseItemList that all model lists should extend
 *
 * @author Nathan Krueger <nathan.krueger@blueprintagencies.com>
 * @package base
 */

class DatabaseList extends DatabaseItemList
{

    private $queryCreated;

    private $pkgHandle = 'base';

    /**
     * Anyone who extends the DatabaseList class must have a model defined at the least
     */
    public function __construct()
    {
        /**
         * Must have TABLE NAME and MODEL NAME constant
         */
    }

    protected function setBaseQuery()
    {
        $this->setQuery("SELECT * FROM " . $this::TABLE_NAME);
    }

    protected function createQuery()
    {
        if (!$this->queryCreated) {
            $this->setBaseQuery();
            $this->queryCreated = 1;
        }
    }

    public function get($itemsToGet = 0, $offset = 0)
    {
        $model = $this::MODEL_NAME;
        $objects = array();
        $this->createQuery();
        $r = parent::get($itemsToGet, $offset);
        foreach ($r as $row) {
            $objects[] = new $model(null, $row);
        }
        return $objects;
    }

    /**
     *  filterByMultipleValues
     *
     * This method will go through an array and produce a query to check for the value
     * in the column provide using the logic operator 'OR'.
     *
     * @param (string) $col - Name of database column
     * @param (array) $values - All values to be checked in column
     * @param (bool) $strict - If false, values will be joined using LIKE
     *
     * @return (void)
     */
    public function filterByMultipleValues($col, $values = array(), $strict = true)
    {
        $db = Loader::db();
        if ($strict) {
            $query = $col . ' IN ' . '(';
        } else {
            $query = "";
        }

        $numValues = count($values);
        $currentValue = 0;

        foreach ($values as $value) {
            if ($strict) {
                $qValue = $db->quote($value);
                if (++$currentValue === $numValues) {
                    //Last value
                    $query .= $qValue . ')';
                } else {
                    //Not last value
                    $query .= $qValue . ', ';
                }
            } else {
                $qValue = $db->quote("%" . $value . "%");
                if (++$currentValue === $numValues) {
                    //Last value
                    $query .= $col . ' LIKE ' . $qValue;
                } else {
                    //Not last value
                    $query .= $col . ' LIKE ' . $qValue . ' OR ';
                }
            }
        }
    }

    public function getTotal()
    {
        $this->createQuery();
        return parent::getTotal();
    }

}
