<?php defined('C5_EXECUTE') or die("Access Denied");

/**
 * @package Base
 * @author Nathan Krueger <nate0k@gmail.com>
 */

abstract class DatabaseObject extends Object
{

    const TABLE_NAME = null;

    // Package handle, can be used with Loader
    const PKG_HANDLE = 'base';

    // Used to filter data in self::setParameters(...)
    protected $ignoredFields = array();

    // The ID should never be being set manuallyy
    protected $id;

    protected $exists = false;

    public function __construct($id = null, $row = null)
    {
        $db = Loader::db();

        // TABLE_NAME must be defined for EVERY model that extends DatabaseObject
        $class = get_called_class();
        $Ref = new ReflectionClass($class);
        if (!$Ref->hasConstant('TABLE_NAME')) {
            throw new Exception("Constant TABLE_NAME not defined for class: {$class}", 1);
        }
        // Check to make sure $ignoredFields is set properly if set
        if (!is_array($this->ignoredFields)) {
            throw new Exception("\$ignoredFields in class `" . $class . "` must be an Array", 1);
        }

        // Ensure it is protected
        $RP = new ReflectionProperty($this, 'ignoredFields');
        if(!$RP->isProtected()){
            throw new Exception("\$ignoredFields in class `" . $class . "` must be a protected property", 1);
        }

        // Setting an ID takes priority over $row
        if ($id != null) {
            $this->id = $id;
            $row = $db->GetRow("SELECT * FROM `" . $class::TABLE_NAME . "` WHERE id = ?", array($id));
        }

        if (!empty($row)) {
            // If we load a row from a table with "is_deleted" field, that must be taken into account
            if (array_key_exists('is_deleted', $row)) {
                // If the value is 0, it hasn't been "soft deleted" yet
                $this->exists = ($row['is_deleted'] == 0);
            } else {
                $this->exists = true;
            }
			
            $this->setPropertiesFromArray($row);
        }
    }

    /**
     * This is a magic PHP function. If a property is not publicly accessible it will check for
     * a 'get' function for that property, following a strict naming convention.
     *
     * Example:
     * If you wanted to access the protected/private property "my_first_property" to be accessed with a method,
     *
     * 1. List it as a protected or private method
     * 2. Create a public function "getMyFirstProperty" (for EXAMPLE)
     * 3. Do what you need to do within that function
     *
     * It is not good practice to allow these functions access to the property if it is private or protected.
     * That is why a method must be added to the class as needed. (Just so everyone knows what is going on :) )
     */
    final public function __get($property)
    {
        $fh = Loader::helper('format', self::PKG_HANDLE);
        $method = 'get' . $fh->toCamelCase($property);

        if (method_exists($this, $method)) {
            return $this->$method();
        }
    }

    /**
     * Same idea as above. This is a magic PHP function and will only be accessed when attempting to set
     * a private, protected or unknown property (confirm)
     */
    final public function __set($property, $value)
    {
        $fh = Loader::helper('format', self::PKG_HANDLE);
        $method = 'set' . $fh->toCamelCase($property);

        // If a setter method exists, use it
        if (method_exists($this, $method)) {
            return $this->$method($value);
        } else {
            throw new Exception('Cannot SET private or protected property: $' . $property . ',<br/>In class: ' . get_class($this));
        }
    }

    /**
     * Finds all records of {$this} object in the database
     *
     * Can accept 1 condition key(db_column)-value pair. The value to filter can be an array.
     *
     * @param (string) $col - Column name
     * @param (mixed) $val - Column value to filter
     * @return (array) $objects
     */
    public static function findAll($col = '', $val = null)
    {
        $class = get_called_class();
        $db = Loader::db();

        // $col must be provided in order to query with a value since null is acceptable
        if (empty($col)) {
            $results = $db->GetAll("SELECT * FROM `" . $class::TABLE_NAME . "`");
        } else {
            if (is_array($val)) {
                // Must use double quotes
                $queryString = '"' . implode('","', $val) . '"';
                $results = $db->GetAll("SELECT * FROM `" . $class::TABLE_NAME . "` WHERE {$col} in ({$queryString})");
            } else {
                $results = $db->GetAll("SELECT * FROM `" . $class::TABLE_NAME . "` WHERE {$col} = ?", array($val));
            }
        }
        $objects = array();
        if ($results) {
            foreach ($results as $row) {
                $objects[] = new $class(null, $row);
            }
        }
        return $objects;
    }

    /**
     * Creates a KEY VALUE array of any two columns from the database. Conditions are optional
     *
     * @param (string) $key - The key column to be used
     * @param (string) $value - The value column to be used
     * @param (string) $col - The name of the column in the condition
     * @param (mixed) $colVal - The value the column should have
     * @return (mixed) FALSE if select failed. Array if it succeeded
     */
    public static function findAssoc($key, $value, $col = null, $colVal = null)
    {
        $class = get_called_class();
        $db = Loader::db();
        if (empty($col)) {
            $results = $db->GetAssoc("SELECT {$key},{$value} FROM `" . $class::TABLE_NAME . "`");

        } else {
            if (is_array($colVal)) {
                // Quote all values
                foreach ($colVal as $key => $value) {
                    $colVal[$key] = $db->qstr($value);
                };
                // Must use double quotes
                $queryString = '"' . implode('","', $colVal) . '"';
                $results = $db->GetAssoc("SELECT {$key},{$value} FROM `" . $class::TABLE_NAME . "` WHERE {$col} in ({$queryString})");
            } else {
                $results = $db->GetAssoc("SELECT {$key},{$value} FROM `" . $class::TABLE_NAME . "` WHERE {$col} = ?", array($colVal));
            }
        }
        return $results ? $results : array();
    }

    /**
     * Plain and simple, selects a single column from the objects table.
     * Conditions are optional same sorta idea applies like above
     *
     * @param (string) $one - THE ONE COLUMN TO RULE THEM ALL
     * @param (string) $col - Column of the conditional statement
     * @param (mixed) $val  - Value to query for
     */
    public static function findCol($one, $col = null, $val = null)
    {
        $class = get_called_class();
        $db = Loader::db();
        if (empty($col)) {
            $row = $db->GetCol("SELECT {$one} FROM `" . $class::TABLE_NAME . "`");
        } else {
            if (is_array($val)) {
                // Quote all values
                foreach ($val as $key => $value) {
                    $val[$key] = $db->qstr($value);
                };
                // Must use double quotes to enclose
                $queryString = '"' . implode('","', $val) . '"';
                $row = $db->GetCol("SELECT {$one} FROM `" . $class::TABLE_NAME . "` WHERE {$col} in ({$queryString})");
            } else {
                $row = $db->GetCol("SELECT {$one} FROM `" . $class::TABLE_NAME . "` WHERE {$col} = ?", array($val));
            }
        }
        return ($row) ? $row : array();
    }

    /**
     * A static call may be made to this method only if an array of data is passed
     * to it. Otherwise the object instanciated can call save upon itself
     *
     * @return (int) - ID of record in the database
     */
    public function save()
    {
        $class = get_called_class();
        $db = Loader::db();

        // Attempt to find the object in the database currently
        $Singleton = $this->findSingleton();

        $preparedData = get_object_vars($this);
        // Unset ID so we don't override fields
        unset($preparedData['id']);

        $u = new User();

        // Properly configured timezone
        date_default_timezone_set('America/Toronto');

        //The neat thing about AutoExecute is that it will only save fields it matches in the database from the preparedData
        if (!is_object($Singleton)) {
            //Since we are inserting, if a table has created fields, add to them
            $preparedData['created'] = date('Y-m-d H:i:s');
            $preparedData['created_by'] = $u->getUserId();
            // Insert the object to its respective table
            $db->AutoExecute($class::TABLE_NAME, $preparedData, 'INSERT');
            // Set the ID for the object, it now exists too
            $this->id = $db->Insert_ID();
            $this->exists = TRUE;
            return $db->Insert_ID();
        } else {
            $preparedData['modified'] = date('Y-m-d H:i:s');
            $preparedData['modified_by'] = $u->getUserId();
            //We don't want the created or created_by fields being modified
            unset($preparedData['created']);
            unset($preparedData['created_by']);
            //Update the object we found and return the database ID
            $db->AutoExecute($class::TABLE_NAME, $preparedData, 'UPDATE', 'id = ' . $Singleton->id);
            return $Singleton->id;
        }
    }

    /**
     *  Given a key value array, assigns all permitted fields to the current object.
     * Permitted fields are determined by a classes $ignoredFields parameter
     */
    public function setParams($data)
    {
        // If there are no fields to ignore, use $data. Otherwise, ignore keys in $data
        $preparedData = (!empty($this->ignoredFields)) ? array_diff_key($data, array_flip($this->ignoredFields)) : $data;
        // Fetch all the public properties
        $classProps = call_user_func('get_object_vars', $this);
        // From our $preparedData, we can only accept "legal parameters"
        // Intersection of array keys on $classProps and $data will give us all the parameters we
        // are able to set from $data with respect to visibility
        $legalParams = array_intersect_key($classProps, $data);
        // First param: $preparedData, is the data we wish to preserve
        // Second param: $legalParams, is filtering out all the keys not in the first param
        $saveData = array_intersect_key($preparedData, $legalParams);
        $this->setPropertiesFromArray($saveData);
    }

    /**
     *  Attempt to find a unique object from the datbase. By default we want to return the object given
     * if it has an ID since we know that ID's are unique to all models.
     * The main purpose of this method is to be extended if needed, since some tables may have keypair
     * primary key. This method ALSO allows you to change the primary key if you don't wish to use "id"
     *
     * @return (mixed) - Object should be returned if a unique one is found, otherwise not an object
     */
    public function findSingleton()
    {
        if ($this->exists()) {
            return $this;
        } else {
            return null;
        }
    }

    /**
     * Gives the user the ability to permanently delete a record, or make it "inactive"
     * essentially. In order to use a soft delete, there must be a "is_deleted" (tinyint) field.
     *
     * @param (string) $type - one of: "hard", "soft"
     * @return (bool)
     */
    public function delete($type)
    {
        $class = get_called_class();
        if (!in_array($type, array('soft', 'hard'))) {
            throw new Exception("Invalid delete type", 1);
        }

        $db = Loader::db();
        $Singleton = $this->findSingleton();
        if (is_object($Singleton)) {
            if ($type == 'soft') {
                if (!property_exists($this, 'is_deleted')) {
                    throw new Exception("Class " . get_class($this) . " has no property is_deleted. Soft delete cannot be performed");
                }
                $results = $db->Execute("UPDATE `" . $class::TABLE_NAME . "` SET is_deleted = 1 WHERE id = ? ", array($Singleton->id));
            } else {
                $results = $db->Execute("DELETE FROM `" . $class::TABLE_NAME . "` WHERE id = ? LIMIT 1", array($Singleton->id));
            }
            if ($results) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Saves an array of objects all of the same type to its respective table
     *
     * @param (array) - Objects of all the same type
     */
    public function saveAll()
    {
        foreach ($objects as $Object) {
            $this->save();
        }
    }

    /**
     * This method should be use to determine if an object exists.
     *
     * @return (bool) - true if object exists, false if it does not
     */
    public function exists()
    {
        return $this->exists;
    }

    /**
     * Since the id property is not publicly accessible, our magic methods allow us to
     * still access it through this method.
     *
     * @return (mixed) - ID of the object if any
     */
    public function getId()
    {
        return $this->id;
    }

}
